<h1 align="center">
   <strong>next</strong>
</h1>

<p align="center">
    <a href="https://codecov.io/gh/challisa/next" target="_blank">
        <img src="https://img.shields.io/codecov/c/github/challisa/next?color=%2334D058" alt="Coverage">
    </a>
    <a href="https://challisa.github.io/next" target="_blank">
        <img src="https://img.shields.io/badge/docs-mkdocs%20material-blue.svg?style=flat" alt="Docs">
    </a>
    <a href="https://pypi.org/project/next/" target="_blank">
        <img src="https://img.shields.io/pypi/v/next.svg" alt="PyPI Latest Release">
    </a>
    <br /><a href="https://gitlab.com/challisa/next/-/blob/main/LICENSE" target="_blank">
        <img src="https://img.shields.io/gitlab/license/challisa/next.svg" alt="License">
    </a>
    <a href="https://github.com/psf/black" target="_blank">
        <img src="https://img.shields.io/badge/code%20style-black-000000.svg" alt="Code style: black">
    </a>
</p>

...

## Main Features

- ... # TODO: Add features

## Installation

<div class="termy">

```console
$ pip install next

---> 100%
```

</div>

## Usage

### Basic 😊

```python
import next

...
```

Results in ... # TODO: Add example

```python
# TODO: Add example
```

## Credits

- # TODO: Add credits

## License

* [MIT License](/LICENSE)
